package com.epamcourses.orestonatsko.warehouse;

import java.math.BigDecimal;
import java.util.List;

public interface Manager {
    Goods findGoods(String name,  BigDecimal commission, List<?extends Goods> goods);

    Goods findGoods(int id, BigDecimal commission, List<?extends Goods> goods);

    boolean addGoods(List<?extends Goods> newGoods, List< Goods> goods);

    void makeAccounting(List<Goods> goods, BigDecimal commission);

    List<Goods> getGoods(BigDecimal commission, List<? extends Goods> goods);

}
