package com.epamcourses.orestonatsko.warehouse.stuffwarehouse;

import com.epamcourses.orestonatsko.warehouse.Manager;
import com.epamcourses.orestonatsko.warehouse.Goods;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class StuffWarehouse {
    private String name;
    private List<Goods> goods;
    private Manager warehouseManager;
    private BigDecimal commission;

    public StuffWarehouse(String name, BigDecimal commission) {
        this.name = name;
        this.commission = commission;
        this.goods = new ArrayList<>();
    }

    public StuffWarehouse(String name, BigDecimal commission, List<Goods> goods) {
        this.name = name;
        this.commission = commission;
        this.goods = new ArrayList<>();
        this.goods.addAll(goods);
    }

    public boolean addGoods(List<Goods> goods) {
       return warehouseManager.addGoods(goods, this.goods);
    }

    public List<Goods> getGoods(){
        return warehouseManager.getGoods(commission, goods);
    }

    public void makeAccounting() {
        warehouseManager.makeAccounting(goods, commission);
    }

    public Goods findGoods(String name) {
        return warehouseManager.findGoods(name, commission, goods);
    }

    public Goods findGoods(int id) {
        return warehouseManager.findGoods(id, commission, goods);
    }

    public void setManager(Manager manager) {
        warehouseManager = manager;
    }

    @Override
    public String toString() {
        return name;
    }
}
