package com.epamcourses.orestonatsko.warehouse.stuffwarehouse;

import com.epamcourses.orestonatsko.warehouse.Manager;
import com.epamcourses.orestonatsko.warehouse.Goods;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WarehouseManager implements Manager {

    @Override
    public Goods findGoods(String name, BigDecimal commission, List<? extends Goods> goods) {
        Goods copy = null;
        try {
            copy = Objects.requireNonNull(goods.stream().filter(g1 -> name.equals(g1.getName())).findFirst()
                    .orElse(null)).clone();
            copy.setCost(calcCost(copy.getCost(), commission));
            return copy;
        } catch (CloneNotSupportedException | NullPointerException e) {
//            e.printStackTrace();
        }
        return copy;
    }

    @Override
    public Goods findGoods(int id, BigDecimal commission, List<? extends Goods> goods) {
        Goods copy = null;
        try {
            copy = Objects.requireNonNull(goods.stream().filter(g -> g.getId() == id).findAny().orElse(null)).clone();
            copy.setCost(calcCost(copy.getCost(), commission));
        } catch (CloneNotSupportedException | NullPointerException e) {
//            e.printStackTrace();
        }
        return copy;
    }

    @Override
    public boolean addGoods(List<? extends Goods> newGoods, List<Goods> goods) {
       return goods.addAll(newGoods);
    }

    @Override
    public void makeAccounting(List<Goods> goods, BigDecimal commission) {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(String.valueOf(this.getClass().getResourceAsStream("AccountingReport")))))) {
            writer.write("\t\t\t--Accounting report--");
            writer.newLine();
            for (Goods g : goods) {
                String strGoods = String.format("%s; Real cost: $%-8.2f; StuffWarehouse commission: $%.2f; Total: $%.2f",
                        g.getName(), g.getCost(), calcCommission(g.getCost(), commission), calcCost(g.getCost(), commission));
                writer.write(strGoods);
                writer.newLine();
            }

        } catch (IOException e) {
//            e.printStackTrace();
        }
    }

    @Override
    public List<Goods> getGoods( BigDecimal commission, List<? extends Goods> goods) {
        List<Goods> goodsList = new ArrayList<>();
        goods.forEach(g -> {
            try {
                Goods copy = g.clone();
                copy.setCost(calcCost(copy.getCost(), commission));
                goodsList.add(copy);
            } catch (CloneNotSupportedException e) {
//                e.printStackTrace();
            }
        });
        return goodsList;
    }
    private BigDecimal calcCommission(BigDecimal cost, BigDecimal commission) {
        return cost.multiply(commission);
    }

    private BigDecimal calcCost(BigDecimal cost, BigDecimal commission) {
        return cost.multiply(commission).add(cost);
    }
}
