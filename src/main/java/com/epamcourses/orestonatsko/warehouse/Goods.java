package com.epamcourses.orestonatsko.warehouse;

import java.math.BigDecimal;

public class Goods implements Cloneable {
    private static int count;
    private int id;
    private String name;
    private BigDecimal cost;

    public Goods(String name, BigDecimal cost) {
        this.name = name;
        this.cost = cost;
        id = ++count;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return String.format("id: %2d - %s, $%.2f;", id, name, cost);
    }

    @Override
    public Goods clone() throws CloneNotSupportedException {
        return (Goods) super.clone();
    }
}
