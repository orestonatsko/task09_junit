package com.epamcourses.orestonatsko;

import com.epamcourses.orestonatsko.menu.Menu;
import com.epamcourses.orestonatsko.warehouse.Manager;
import com.epamcourses.orestonatsko.warehouse.stuffwarehouse.StuffWarehouse;
import com.epamcourses.orestonatsko.warehouse.stuffwarehouse.WarehouseManager;

import java.math.BigDecimal;

public class App {
    public static void main(String[] args) {
        double commission = 0.22;
        StuffWarehouse warehouse = new StuffWarehouse("Stuff warehouse", new BigDecimal(commission));
        Manager manager = new WarehouseManager(); //let's assume that we did not implement it
        warehouse.setManager(manager);
        WarehouseController controller = new WarehouseController(Menu.getInstance(), warehouse);
        controller.run();
    }
}
