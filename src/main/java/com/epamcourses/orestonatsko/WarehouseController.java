package com.epamcourses.orestonatsko;

import com.epamcourses.orestonatsko.menu.Menu;
import com.epamcourses.orestonatsko.menu.MenuItem;
import com.epamcourses.orestonatsko.warehouse.Goods;
import com.epamcourses.orestonatsko.warehouse.stuffwarehouse.StuffWarehouse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WarehouseController {
    private Menu menu;
    private StuffWarehouse warehouse;

    public WarehouseController(Menu menu, StuffWarehouse warehouse) {
        this.menu = menu;
        this.warehouse = warehouse;
    }

    void run() {
        warehouse.addGoods(getGoods());
        generateMenu();
        menu.show();
    }

    private void generateMenu() {
        menu.addMenuItem(new MenuItem("show all goods") {
            @Override
            public void execute() {
                menu.display(warehouse.getGoods());
            }
        });
        menu.addMenuItem(new MenuItem("find goods by name") {
            @Override
            public void execute() {
                findGoodsByName();
            }
        });
        menu.addMenuItem(new MenuItem("Create purchase") {
            @Override
            public void execute() {
                createPurchase();
            }
        });
        menu.addMenuItem(new MenuItem("Create accounting report") {
            @Override
            public void execute() {
                warehouse.makeAccounting();
            }
        });
    }

    private void createPurchase() {
        String input;
        List<Goods> soldGoods = new ArrayList<>();
        do {
            menu.display("Select good's");
            menu.display(warehouse.getGoods());
            menu.display("Q - Quit");
            menu.display("Enter good's id:");
            input = menu.getInput();
            try {
                int id = Integer.valueOf(input);
                Goods g = warehouse.findGoods(id);
                if (g != null)
                    soldGoods.add(g);
            } catch (NumberFormatException e) {
//                e.printStackTrace();
            }
        } while (!input.equalsIgnoreCase("Q"));
        menu.display("\tShopping cart:");
        menu.display(soldGoods);
    }

    private void findGoodsByName() {
        menu.display("Enter name of goods:");
        String name = menu.getInput();
        Goods goods = warehouse.findGoods(name);
        if (goods != null) {
            menu.display(String.format("name: %s; cost: $%.2f", goods.getName(), goods.getCost()));
        } else {
            menu.display("There is no good's in the warehouse");
        }
    }

    private List<Goods> getGoods() {
        return Arrays.asList(
                new Goods("book", BigDecimal.valueOf(55.05)),
                new Goods("book", BigDecimal.valueOf(55.05)),
                new Goods("big book", BigDecimal.valueOf(88.73)),
                new Goods("beg", BigDecimal.valueOf(133.3)),
                new Goods("table", BigDecimal.valueOf(56.05)),
                new Goods("shirt", BigDecimal.valueOf(35.56)),
                new Goods("pants", BigDecimal.valueOf(145.85)),
                new Goods("coca-cola", BigDecimal.valueOf(2.99)),
                new Goods("note", BigDecimal.valueOf(33.9)),
                new Goods("flower", BigDecimal.valueOf(5.66))
        );
    }
}
