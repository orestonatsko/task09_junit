package com.epamcourses.orestonatsko.menu;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Menu {

    private Map<String, MenuItem> items;
    private static int itemCount;
    private Scanner input = new Scanner(System.in);
    private static Menu menu;

    private Menu() {
    }

    public void addMenuItem(MenuItem item) {
        if (items == null) {
            items = new TreeMap<>();
        }
        items.put(String.valueOf(++itemCount), item);
    }

    public void show() {
        if (items != null) {
            String key;
            do {
                System.out.println();
                System.out.println("\t--MAIN MENU--");
                items.forEach((k, v) -> System.out.println(k + " - " + v.getName()));
                System.out.println("Q - Quit");
                key = input.next();
                MenuItem item = items.get(key);
                if (item != null) {
                    item.execute();
                }
            } while (!key.toUpperCase().equals("Q"));
        } else {
            System.out.println("--MENU IS EMPTY!--");
        }
    }

    public String getInput() {
        Scanner input = new Scanner(System.in);
        return input.next();
    }

    public void display(String msg) {
        System.out.println(msg);
    }

    public void display(List<?> list) {
        list.forEach(System.out::println);
    }

    public static Menu getInstance() {
        if (menu == null) {
            menu = new Menu();
        }
        return menu;
    }
}
