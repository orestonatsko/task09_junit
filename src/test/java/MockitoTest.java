import com.epamcourses.orestonatsko.warehouse.Goods;
import com.epamcourses.orestonatsko.warehouse.Manager;
import com.epamcourses.orestonatsko.warehouse.stuffwarehouse.StuffWarehouse;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
@RunWith(MockitoJUnitRunner.class)
public class MockitoTest {

    private static List<Goods> goods;
    private static double commission = 0.25;

    @InjectMocks
    private static StuffWarehouse warehouse;

    @Mock
    private static Manager manager;

    @BeforeClass
    public static void generateGoods() {
        goods = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            goods.add(new Goods("old item" + i, new BigDecimal(i * 3.4)));
        }

    }

    @BeforeClass
    public static void initWarehouse() {
        warehouse = new StuffWarehouse("test warehouse", new BigDecimal(commission), goods);
        warehouse.setManager(manager);
    }

    @Test
    public void getGoodsTest() {
        List<Goods> gotGoods = warehouse.getGoods();
        when(manager.getGoods(BigDecimal.valueOf(0.25), goods)).thenReturn(gotGoods);
    }

    @Test
    public void findGoodsTest() {
        String name = warehouse.getGoods().get(0).getName();
        Goods testGoods = new Goods(name, BigDecimal.valueOf(20.0));

        when(manager.findGoods(name, BigDecimal.valueOf(commission) , goods )).thenReturn(new Goods(name,
                BigDecimal.valueOf((20.0 * commission) + 20.0)));

        Assert.assertNotEquals(testGoods.getCost(), testGoods.getCost());
    }
}
