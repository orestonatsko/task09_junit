import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({WarehouseTest.class, WarehouseManagerTest.class, WarehouseTestWithMockito.class})
public class SuitClassTest {
}
