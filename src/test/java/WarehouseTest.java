import com.epamcourses.orestonatsko.warehouse.Goods;
import com.epamcourses.orestonatsko.warehouse.stuffwarehouse.StuffWarehouse;
import com.epamcourses.orestonatsko.warehouse.stuffwarehouse.WarehouseManager;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WarehouseTest {
    private static List<Goods> goods;
    private static List<Goods> newGoods;
    private static StuffWarehouse warehouse;

    @BeforeClass
    public static void generateGoods() {
        goods = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            goods.add(new Goods("old item" + i, new BigDecimal(i * 3.4)));
        }
        newGoods = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            newGoods.add(new Goods("new item" + i, new BigDecimal(i * 3.4)));
        }
    }

    @BeforeClass
    public static void initWarehouse() {
        double commission = 0.25;
        WarehouseManager manager = new WarehouseManager();
        warehouse = new StuffWarehouse("test warehouse", new BigDecimal(commission), goods);
        warehouse.setManager(manager);
    }

    @Test
    public void addGoodsTest() {
        int oldLength = warehouse.getGoods().size();
        boolean added = warehouse.addGoods(newGoods);
        int newLength = warehouse.getGoods().size();

        assertTrue(added);
        assertNotEquals(oldLength, newLength);
        assertEquals(warehouse.getGoods().size(), goods.size() + newGoods.size());
    }

    @Test
    public void getGoodsTest() {
        List<Goods> gotGoods = warehouse.getGoods();
        assertNotNull(gotGoods);
        assertNotSame(warehouse.getGoods(), gotGoods);
        assertEquals(gotGoods.size(), warehouse.getGoods().size(), 0.0);
    }

    @Test
    public void findGoodsTest(){
        String name = "test";
        Goods testGoods = warehouse.findGoods(name);

        Assume.assumeNotNull(testGoods); // just tried
        assertNotNull(testGoods);

        name = warehouse.getGoods().get(0).getName();
        testGoods = warehouse.findGoods(name);

        assertNotNull(testGoods);
        Assert.assertNotEquals(goods.get(0).getCost(), testGoods.getCost());
    }

}
