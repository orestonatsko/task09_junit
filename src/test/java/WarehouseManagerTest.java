import com.epamcourses.orestonatsko.warehouse.stuffwarehouse.WarehouseManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class WarehouseManagerTest {
    @Parameterized.Parameter
    public BigDecimal commission;
    @Parameterized.Parameter(1)
    public BigDecimal cost;
    @Parameterized.Parameter(2)
    public BigDecimal warCommission;
    @Parameterized.Parameter(3)
    public BigDecimal result;

    @Test
    public void calcCommissionTest() {
        try {
            Method calcCommissionMethod = WarehouseManager.class.getDeclaredMethod("calcCommission",
                    BigDecimal.class, BigDecimal.class);
            calcCommissionMethod.setAccessible(true);
            BigDecimal warCommission = (BigDecimal) calcCommissionMethod.invoke(new WarehouseManager(), cost, commission);
            Assert.assertNotNull(warCommission);
            Assert.assertEquals(warCommission, this.warCommission);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void calcCostTest() {
        try {
            Method calcCostTest = WarehouseManager.class.getDeclaredMethod("calcCost",
                    BigDecimal.class, BigDecimal.class);
            calcCostTest.setAccessible(true);
            BigDecimal result = (BigDecimal) calcCostTest.invoke(new WarehouseManager(), cost, commission);
            Assert.assertNotNull(result);
            Assert.assertEquals(result, this.result);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getTestData() {
        return Arrays.asList(new Object[][]{
                {BigDecimal.valueOf(0.25),BigDecimal.valueOf(99.99), BigDecimal.valueOf(24.9975), BigDecimal.valueOf(124.9875)},
                {BigDecimal.valueOf(0.1),BigDecimal.valueOf(5), BigDecimal.valueOf(0.5),BigDecimal.valueOf(5.5) },
                {BigDecimal.valueOf(1.25),BigDecimal.valueOf(22.1), BigDecimal.valueOf(27.625), BigDecimal.valueOf(49.725)},
                {BigDecimal.valueOf(0.01),BigDecimal.valueOf(0.5), BigDecimal.valueOf(0.005), BigDecimal.valueOf(0.505)},
                {BigDecimal.valueOf(0.99),BigDecimal.valueOf(11.1), BigDecimal.valueOf(10.989), BigDecimal.valueOf(22.089)},
                {BigDecimal.valueOf(0.25),BigDecimal.valueOf(11.1), BigDecimal.valueOf(2.775), BigDecimal.valueOf(13.875)},
        });
    }
}